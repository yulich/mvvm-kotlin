package com.jeff.viewmodel.viewModel

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.jeff.viewmodel.model.DataModel


class MainViewModel : ViewModel() {
    private val mDataModel = DataModel().also { Log.d("Jeff", "init Model") }

    val titleOb = ObservableField<String>()

    fun refresh(count1: Int, count2: Int) {
        mDataModel.retrieveData(object : DataModel.OnDataReadyCallback {
            override fun onDataReady(data: String) {
                titleOb.set(String.format("%s, %d, %d", data, count1, count2))
            }
        })
    }
}