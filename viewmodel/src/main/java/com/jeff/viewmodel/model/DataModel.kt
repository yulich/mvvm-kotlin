package com.jeff.viewmodel.model

import android.os.Handler

class DataModel {

    interface OnDataReadyCallback {
        fun onDataReady(data: String)
    }

    fun retrieveData(callback: OnDataReadyCallback) {
        Handler().postDelayed({
            callback.onDataReady("Jeff")
        }, 1000)
    }
}