package com.jeff.viewmodel.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.jeff.viewmodel.R
import com.jeff.viewmodel.databinding.ActivityMainBinding
import com.jeff.viewmodel.viewModel.MainViewModel

private var count = 0

class MainActivity : AppCompatActivity() {

    /**
     * lateninit 修飾詞, replace avoid '!!'
     * private var binding: ActivityMainBinding? = null
     */
    private lateinit var binding: ActivityMainBinding

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // 透過'ViewModelProviders'取得'ViewModel'
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.mainvm = viewModel

        // view is never used, could be replace to _
        // binding.btnMe.setOnClickListener{ view -> viewModel.refresh(Singleton.instance.number++) }
        binding.btnMe.setOnClickListener{ _ -> viewModel.refresh(++count, ++count) }

        findViewById<TextView>(R.id.tvMerge).also { it.text = viewModel.titleOb.get() }
    }
}
