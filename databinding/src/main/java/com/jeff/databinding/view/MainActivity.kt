package com.jeff.databinding.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.jeff.databinding.R
import com.jeff.databinding.databinding.ActivityMainBinding
import com.jeff.databinding.viewModel.MainViewModel
import com.jeff.databinding.viewModel.Singleton

private var count = 0

class MainActivity : AppCompatActivity() {

    /**
     * lateninit 修飾詞, replace avoid '!!'
     * private var binding: ActivityMainBinding? = null
     */
    private lateinit var binding: ActivityMainBinding

    private val viewModel = MainViewModel().also { Log.d("Jeff", "init ViewModel") }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.mainvm = viewModel

        // 不需要 setContentView
        // setContentView(binding.root)

        // view is never used, could be replace to _
        // binding.btnMe.setOnClickListener{ view -> viewModel.refresh(Singleton.instance.number++) }
        binding.btnMe.setOnClickListener{ _ -> viewModel.refresh(Singleton.instance.number++, ++count) }

        findViewById<TextView>(R.id.tvMerge).also { it.text = viewModel.titleOb.get() }
    }
}
