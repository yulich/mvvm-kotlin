package com.jeff.databinding.viewModel

class Singleton private constructor() {

    init {
        println("Singleton")
    }

    companion object {
        val instance = SingletonHolder.holder
    }

    private object SingletonHolder {
        val holder = Singleton()
    }

    var number = 0
}
