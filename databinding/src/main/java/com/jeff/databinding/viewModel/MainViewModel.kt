package com.jeff.databinding.viewModel

import android.util.Log
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableField
import com.jeff.databinding.model.DataModel

class MainViewModel {
    private val mDataModel = DataModel().also { Log.d("Jeff", "init Model") }

    val titleOb = ObservableField<String>()

    fun refresh(count1: Int, count2: Int) {
        mDataModel.retrieveData(object : DataModel.OnDataReadyCallback {
            override fun onDataReady(data: String) {
                titleOb.set(String.format("%s, %d, %d", data, count1, count2))
            }
        })
    }
}