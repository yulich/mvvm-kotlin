package com.jeff.kotlin.livedata.model

import android.util.Patterns

class LoginUser(val emailAddress: String?, val password: String?) {

    val isEmailValid: Boolean
        get() = Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches()

    val isPasswordLengthGreaterThan5: Boolean
        get() {
            if (password != null) {
               return password.length > 5
            }

            return false
        }
}
