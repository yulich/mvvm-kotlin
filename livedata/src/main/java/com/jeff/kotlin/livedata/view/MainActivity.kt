package com.jeff.kotlin.livedata.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.jeff.kotlin.livedata.R
import com.jeff.kotlin.livedata.databinding.ActivityMainBinding
import com.jeff.kotlin.livedata.model.LoginUser
import com.jeff.kotlin.livedata.viewModel.LoginViewModel
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // 透過ViewModelProviders協助我們取得ViewModel
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.lifecycleOwner = this

        binding.loginViewModel = loginViewModel

        loginViewModel.liveDataLoginUser.observe(this, Observer<LoginUser> { loginUser ->
            when {
                TextUtils.isEmpty(Objects.requireNonNull(loginUser).emailAddress) -> {
                    binding.editEmailAddress.error = "Enter an E-Mail Address"
                    binding.editEmailAddress.requestFocus()
                }
                !loginUser.isEmailValid -> {
                    binding.editEmailAddress.error = "Enter a Valid E-mail Address"
                    binding.editEmailAddress.requestFocus()
                }
                TextUtils.isEmpty(Objects.requireNonNull(loginUser).password) -> {
                    binding.editPassword.error = "Enter a password"
                    binding.editPassword.requestFocus()
                }
                !loginUser.isPasswordLengthGreaterThan5 -> {
                    binding.editPassword.error = "Enter at least 6 Digit password"
                    binding.editPassword.requestFocus()
                }
                else -> {
                    binding.tvEmailAnswer.text = loginUser.emailAddress
                    binding.tvPasswordAnswer.text = loginUser.password
                }
            }
        })
    }
}
