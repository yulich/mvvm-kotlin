package com.jeff.kotlin.livedata.viewModel

import android.view.View

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jeff.kotlin.livedata.model.LoginUser

class LoginViewModel : ViewModel() {

    var liveDataEmailAddress = MutableLiveData<String>().also { it.value = "jeff@mail.com" }
    var liveDataPassword = MutableLiveData<String>().apply { this.value = "123456" }
    var liveDataLoginUser = MutableLiveData<LoginUser>()

    fun onClick(view: View) {
        val loginUser = LoginUser(liveDataEmailAddress.value, liveDataPassword.value)
        liveDataLoginUser.value = loginUser
    }
}
